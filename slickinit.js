jQuery(document).ready(function(){
 jQuery('.slick-slider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    adaptiveHeight: true,
    prevArrow: '<div class="prev">&#x2039;</div>',
    nextArrow: '<div class="next">&#x203a;</div>'
  });
});
